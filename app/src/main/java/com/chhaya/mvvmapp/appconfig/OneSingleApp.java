package com.chhaya.mvvmapp.appconfig;

import android.app.Application;
import android.content.Intent;
import android.util.Log;

import com.chhaya.mvvmapp.di.components.AppComponent;
import com.chhaya.mvvmapp.di.components.DaggerAppComponent;
import com.chhaya.mvvmapp.views.main.MainActivity;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONObject;

public class OneSingleApp extends Application {

    private AppComponent appComponent;

    public AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .setNotificationOpenedHandler(new NotificationOpenedHandler())
                .init();

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        appComponent = DaggerAppComponent.builder().build();
        appComponent.inject(this);
    }

    class NotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {

        @Override
        public void notificationOpened(OSNotificationOpenResult result) {
            // This fires when a notification is opened by tapping it.
            OSNotificationAction.ActionType actionType = result.action.type;
            JSONObject data = result.notification.payload.additionalData;

            String customKey = "";

            Log.i("tag OneSignal", "Result = " + result.notification.payload.toJSONObject());
            Log.i("tag OneSignal", "Data is " + data);

            if (data != null) {
                customKey = data.optString("customkey", null);
                Log.i("tag OneSignal", "Result = " + customKey);
            }

            if (actionType == OSNotificationAction.ActionType.Opened) {
                Intent mainIntent = new Intent(getApplicationContext(), MainActivity.class);
                mainIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                mainIntent.putExtra("owner", customKey);
                startActivity(mainIntent);
            }
        }

    }

}
