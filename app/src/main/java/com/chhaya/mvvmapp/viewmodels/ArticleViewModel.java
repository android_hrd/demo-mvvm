package com.chhaya.mvvmapp.viewmodels;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.chhaya.mvvmapp.models.data.api.entities.Pagination;
import com.chhaya.mvvmapp.models.data.api.repositories.ArticleRepository;
import com.chhaya.mvvmapp.models.data.api.responses.ListArticleResponse;
import com.chhaya.mvvmapp.models.data.api.responses.SingleArticleResponse;

public class ArticleViewModel extends ViewModel {

    private LiveData<ListArticleResponse> articleListObservable;
    private MutableLiveData<Boolean> articleListTrigger;
    private ArticleRepository articleRepository;
    private Pagination paging;

    private MutableLiveData<SingleArticleResponse> articleLiveData;

    public ArticleViewModel() {
        articleRepository = new ArticleRepository();
    }

    public void init() {
        Log.d("tag init", "init is worked");
        articleRepository = new ArticleRepository();
        paging = new Pagination();
        articleListTrigger = new MutableLiveData<>();
        fetchArticles();
        articleListObservable = Transformations.switchMap(articleListTrigger,
                input -> articleRepository.getListArticleByPagination(paging));
    }

    public LiveData<ListArticleResponse> getArticleListObservable() {
        return articleListObservable;
    }

    public LiveData<SingleArticleResponse> getArticleObservable() {
        return articleLiveData;
    }

    public void fetchArticles() {
        articleListTrigger.setValue(true);
    }

    public void getArticleById(int id) {
        articleLiveData = articleRepository.getArticleById(id);
    }

}
