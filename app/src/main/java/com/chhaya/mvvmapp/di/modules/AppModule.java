package com.chhaya.mvvmapp.di.modules;

import androidx.appcompat.app.AppCompatActivity;

import com.chhaya.mvvmapp.models.data.api.repositories.ArticleRepository;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    @Provides
    ArticleRepository provideArticleRepository() {
        return new ArticleRepository();
    }

    @Provides
    AppCompatActivity provideAppCompatActivity(AppCompatActivity appCompatActivity) {
        return appCompatActivity;
    }

}
