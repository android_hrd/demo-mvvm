package com.chhaya.mvvmapp.di.components;

import android.app.Application;

import com.chhaya.mvvmapp.di.modules.AppModule;
import com.chhaya.mvvmapp.viewmodels.ArticleViewModel;

import dagger.Component;

@Component(modules = {AppModule.class})
public interface AppComponent {

    void inject(Application app);

    void inject(ArticleViewModel articleViewModel);

}
