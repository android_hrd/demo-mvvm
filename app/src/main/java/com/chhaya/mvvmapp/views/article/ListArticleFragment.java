package com.chhaya.mvvmapp.views.article;

import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.chhaya.mvvmapp.R;
import com.chhaya.mvvmapp.models.data.api.entities.Article;
import com.chhaya.mvvmapp.utils.CommonUtils;
import com.chhaya.mvvmapp.utils.FragmentUtil;
import com.chhaya.mvvmapp.viewmodels.ArticleViewModel;
import com.chhaya.mvvmapp.views.article.listeners.RecyclerViewListener;

import java.util.ArrayList;
import java.util.List;

public class ListArticleFragment extends Fragment  {

    private ListArticleAdapter adapter;
    private LinearLayoutManager layoutManager;
    private RecyclerView rcvListArticle;
    private SwipeRefreshLayout refreshLayout;

    private ArticleViewModel viewModel;

    /*private int seenFirstVisibleItemPosition = -1;
    private int topView = -1;*/

    private List<Article> articles;

    private boolean isCreated;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(ArticleViewModel.class);

        CommonUtils.showPopupProgressSpinner(getActivity(), true);

        viewModel.init();
        // Observe live data
        observeViewModel(viewModel);
        isCreated = true;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_list_article, container, false);
        rcvListArticle = view.findViewById(R.id.rcv_list_article);
        refreshLayout = view.findViewById(R.id.swipe);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupSwipeRefreshLayout();

        if (!isCreated) {
            Log.d("tag", "it is not a new creating.");
            setupRecyclerView();
        }

    }

    // User defined function
    private void observeViewModel(ArticleViewModel articleViewModel) {
        // Update the list when the data changes
        articleViewModel.getArticleListObservable().observe(this, response -> {
            if (response != null) {
                articles = response.getData();
                setupRecyclerView();
            }
        });
    }

    private void setupRecyclerView() {
        layoutManager = new LinearLayoutManager(getActivity());
        adapter = new ListArticleAdapter(getActivity(), articles);
        rcvListArticle.setLayoutManager(layoutManager);
        rcvListArticle.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        CommonUtils.showPopupProgressSpinner(getActivity(), false);
        if (refreshLayout.isRefreshing())
            refreshLayout.setRefreshing(false);
    }

    private void setupSwipeRefreshLayout() {
        refreshLayout.setOnRefreshListener(() -> {
            // reload data
            refreshLayout.setRefreshing(true);
            viewModel.fetchArticles();
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        isCreated = false;
    }

    /*
    @Override
    public void onPause() {
        super.onPause();
        seenFirstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
        Log.d("last seen", seenFirstVisibleItemPosition + "");
        View startView = rcvListArticle.getChildAt(0);
        topView = (startView == null) ? 0 : (startView.getTop() - rcvListArticle.getPaddingTop());
    }

    @Override
    public void onResume() {
        super.onResume();
        if (seenFirstVisibleItemPosition != -1) {
            layoutManager.scrollToPositionWithOffset(seenFirstVisibleItemPosition, topView);
        }
    }
*/

}
