package com.chhaya.mvvmapp.views.article;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.chhaya.mvvmapp.R;
import com.chhaya.mvvmapp.models.data.api.entities.Article;
import com.chhaya.mvvmapp.models.data.api.responses.SingleArticleResponse;
import com.chhaya.mvvmapp.utils.CommonUtils;
import com.chhaya.mvvmapp.viewmodels.ArticleViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailArticleFragment extends Fragment {

    private ImageView articleImage;
    private TextView articleTitle;
    private TextView articleDesc;

    private int articleId;

    private ArticleViewModel articleViewModel;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.detail_article_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        articleImage = view.findViewById(R.id.detail_article_image);
        articleTitle = view.findViewById(R.id.detail_article_title);
        articleDesc = view.findViewById(R.id.detail_article_desc);

        savedInstanceState = getArguments();
        if (savedInstanceState != null) {
            Log.d("tag", "Result = " + savedInstanceState.getInt("id"));
            articleId = savedInstanceState.getInt("id");
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        articleViewModel = ViewModelProviders.of(this).get(ArticleViewModel.class);
        CommonUtils.showPopupProgressSpinner(getActivity(), true);
        articleViewModel.getArticleById(articleId);
        observeViewModel(articleViewModel);
    }

    private void observeViewModel(ArticleViewModel articleViewModel) {
        // Update the list when the data changes
        articleViewModel.getArticleObservable().observe(this, response -> {
            if (response != null) {
                Log.d("tag", "Result is " + response);
                setupUI(response.getData());
            }
        });
    }

    private void setupUI(Article article) {
        Glide.with(this).load(article.getImage()).into(articleImage);
        articleTitle.setText(article.getTitle());
        articleDesc.setText(article.getDescription());
        CommonUtils.showPopupProgressSpinner(getActivity(), false);
    }
}
