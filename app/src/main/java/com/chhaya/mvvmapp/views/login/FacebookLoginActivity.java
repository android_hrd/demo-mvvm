package com.chhaya.mvvmapp.views.login;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.chhaya.mvvmapp.R;
import com.chhaya.mvvmapp.models.entities.FacebookUser;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FacebookLoginActivity extends AppCompatActivity {

    CallbackManager callbackManager;

    private static final String[] permissions = {"public_profile", "email"};

    @BindView(R.id.fb_login_button)
    LoginButton btnFbLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebook_login);
        ButterKnife.bind(this);

        callbackManager = CallbackManager.Factory.create();

        btnFbLogin.setReadPermissions(Arrays.asList(permissions));

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                setResult(RESULT_OK);
                Log.i("tag success", loginResult + "");
                Log.i("tag success", loginResult.getAccessToken().getToken());
                Log.i("tag success", loginResult.getAccessToken().getUserId());
                Log.i("tag success", loginResult.getAccessToken().toString());

                getFacebookProfile(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                setResult(RESULT_CANCELED);
                Log.i("tag cancel", "is Cancelled");
            }

            @Override
            public void onError(FacebookException error) {
                Log.i("tag error", error + "");
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void getFacebookProfile(AccessToken accessToken) {
        GraphRequest graphRequest = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                Gson gson = new GsonBuilder().create();
                FacebookUser user = gson.fromJson(object.toString(), FacebookUser.class);
                String profile = "";
                try {
                    JSONObject obj = response.getJSONObject().getJSONObject("picture").getJSONObject("data");
                    profile = obj.getString("url");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (user != null) {
                    user.setProfile(profile);
                }

                Log.e("tag profile", user.toString());


            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,first_name,last_name,email,picture.type(large),gender,birthday");
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();
    }
}
