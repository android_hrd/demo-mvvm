package com.chhaya.mvvmapp.views.article;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.chhaya.mvvmapp.R;
import com.chhaya.mvvmapp.models.data.api.entities.Article;
import com.chhaya.mvvmapp.utils.FragmentUtil;
import com.chhaya.mvvmapp.views.article.listeners.RecyclerViewListener;

public class ListArticleActivity extends AppCompatActivity implements RecyclerViewListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);

        ListArticleFragment articleFragment = new ListArticleFragment();
        FragmentUtil.add(this, R.id.container_fragment, articleFragment);

        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:{
                Log.d("tag", "action setting");
            }
            case R.id.action_favorite:{
                Log.d("tag", "action fav");
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onItemClicked(int position, Article data) {
        DetailArticleFragment detailArticleFragment = new DetailArticleFragment();
        Bundle args = new Bundle();
        args.putInt("id", data.getId());
        detailArticleFragment.setArguments(args);
        FragmentUtil.replace(this, R.id.container_fragment, detailArticleFragment);
    }
}
