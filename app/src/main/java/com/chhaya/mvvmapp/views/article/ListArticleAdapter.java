package com.chhaya.mvvmapp.views.article;

import android.content.Context;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.chhaya.mvvmapp.R;
import com.chhaya.mvvmapp.models.data.api.entities.Article;
import com.chhaya.mvvmapp.views.article.listeners.RecyclerViewListener;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ListArticleAdapter extends RecyclerView.Adapter<ListArticleAdapter.ArticleViewHolder> {

    private List<Article> dataSet;
    private Context context;

    private RecyclerViewListener listener;

    public ListArticleAdapter(Context context, List<Article> dataSet) {
        this.context = context;
        this.dataSet = dataSet;
        this.listener = (RecyclerViewListener) context;
    }

    public void setDataSet(List<Article> dataSet) {
        this.dataSet = dataSet;
    }

    public List<Article> getDataSet() {
        return dataSet;
    }

    @NonNull
    @Override
    public ArticleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.list_article_item_layout, parent, false);
        return new ArticleViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ArticleViewHolder holder, int position) {
        Glide.with(context).load(dataSet.get(position).getImage()).placeholder(R.drawable.dota2).into(holder.imageArticle);
        holder.textArticleTitle.setText(dataSet.get(position).getTitle());
        holder.textArticleDesc.setText(dataSet.get(position).getDescription());
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    class ArticleViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView imageArticle;
        private TextView textArticleTitle;
        private TextView textArticleDesc;
        //private TextView textEditAction;
        //private TextView textDelAction;

        public ArticleViewHolder(@NonNull View itemView) {
            super(itemView);
            imageArticle = itemView.findViewById(R.id.list_article_image);
            textArticleTitle = itemView.findViewById(R.id.list_article_title);
            textArticleDesc = itemView.findViewById(R.id.list_article_description);
            //textEditAction = itemView.findViewById(R.id.text_edit_article);
            //textDelAction = itemView.findViewById(R.id.text_delete_article);

            itemView.setOnClickListener(v -> {
                listener.onItemClicked(getAdapterPosition(), dataSet.get(getAdapterPosition()));
            });

        }

    }
}
