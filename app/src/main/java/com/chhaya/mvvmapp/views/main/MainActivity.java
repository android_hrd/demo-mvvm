package com.chhaya.mvvmapp.views.main;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.chhaya.mvvmapp.R;
import com.chhaya.mvvmapp.views.article.ListArticleActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {


    @BindView(R.id.button_start)
    Button btnStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        btnStart.setOnClickListener(v -> {
            Intent listArticleIntent = new Intent(this, ListArticleActivity.class);
            startActivity(listArticleIntent);
        });

    }

}
