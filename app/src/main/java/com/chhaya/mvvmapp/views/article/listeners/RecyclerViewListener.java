package com.chhaya.mvvmapp.views.article.listeners;

import com.chhaya.mvvmapp.models.data.api.entities.Article;

public interface RecyclerViewListener {

    void onItemClicked(int position, Article data);

}
