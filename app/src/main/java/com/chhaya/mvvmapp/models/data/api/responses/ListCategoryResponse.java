package com.chhaya.mvvmapp.models.data.api.responses;

import com.chhaya.mvvmapp.models.data.api.entities.Category;
import com.chhaya.mvvmapp.models.data.api.entities.Pagination;

import java.util.List;

public class ListCategoryResponse {

    private String code;
    private String message;
    private List<Category> data;
    private Pagination pagination;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Category> getData() {
        return data;
    }

    public void setData(List<Category> data) {
        this.data = data;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    @Override
    public String toString() {
        return "ListCategoryResponse{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", data=" + data +
                ", pagination=" + pagination +
                '}';
    }
}
