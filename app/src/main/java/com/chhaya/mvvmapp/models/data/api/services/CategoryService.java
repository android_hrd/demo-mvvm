package com.chhaya.mvvmapp.models.data.api.services;


import com.chhaya.mvvmapp.models.data.api.responses.ListCategoryResponse;

import retrofit2.Call;

public interface CategoryService {

    Call<ListCategoryResponse> getAll();

}
