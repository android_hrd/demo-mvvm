package com.chhaya.mvvmapp.models.entities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class OneSignalNotificationObject {

    private String appId;
    private JSONArray playerIds;
    private JSONObject data;
    private JSONObject contents;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public JSONArray getPlayerIds() {
        return playerIds;
    }

    public void setPlayerIds(JSONArray playerIds) {
        this.playerIds = playerIds;
    }

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }

    public JSONObject getContents() {
        return contents;
    }

    public void setContents(JSONObject contents) {
        this.contents = contents;
    }

    public JSONObject toJsonObject() {
        JSONObject object = new JSONObject();
        try {
            object.put("app_id", getAppId());
            object.put("include_player_ids", getPlayerIds());
            object.put("data", getData());
            object.put("contents", getContents());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }

}
