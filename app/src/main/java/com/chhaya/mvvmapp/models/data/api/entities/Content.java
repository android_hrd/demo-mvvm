package com.chhaya.mvvmapp.models.data.api.entities;

import com.google.gson.annotations.SerializedName;

public class Content {
    @SerializedName("en")
    private String english;

    public Content(String english) {
        this.english = english;
    }

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    @Override
    public String toString() {
        return "Content{" +
                "english='" + english + '\'' +
                '}';
    }
}
