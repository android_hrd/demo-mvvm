package com.chhaya.mvvmapp.models.data.api.repositories;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.chhaya.mvvmapp.models.data.api.configuation.ServiceGenerator;
import com.chhaya.mvvmapp.models.data.api.entities.Pagination;
import com.chhaya.mvvmapp.models.data.api.responses.ListArticleResponse;
import com.chhaya.mvvmapp.models.data.api.responses.SingleArticleResponse;
import com.chhaya.mvvmapp.models.data.api.services.ArticleService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleRepository {

    private ArticleService articleService;

    public ArticleRepository() {
        articleService = ServiceGenerator.createService(ArticleService.class);
    }

    public MutableLiveData<ListArticleResponse> getListArticleByPagination(Pagination pagination) {

        final MutableLiveData<ListArticleResponse> data = new MutableLiveData<>();

        articleService.getAll(pagination.getPage(), pagination.getLimit()).enqueue(new Callback<ListArticleResponse>() {
            @Override
            public void onResponse(Call<ListArticleResponse> call, Response<ListArticleResponse> response) {
                if (response.isSuccessful()) {
                    Log.d("tag success", response.body().getMessage());
                    data.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<ListArticleResponse> call, Throwable t) {
                Log.e("tag error", t.getMessage());
            }
        });

        return data;

    }

    public MutableLiveData<SingleArticleResponse> getArticleById(long id) {
        final MutableLiveData<SingleArticleResponse> data = new MutableLiveData<>();

        articleService.getOne(id).enqueue(new Callback<SingleArticleResponse>() {
            @Override
            public void onResponse(Call<SingleArticleResponse> call, Response<SingleArticleResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    Log.d("TAG", response.body().getMessage());
                    data.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<SingleArticleResponse> call, Throwable t) {
                Log.e("TAG ERROR", t.getMessage());
            }
        });

        return data;
    }

}
