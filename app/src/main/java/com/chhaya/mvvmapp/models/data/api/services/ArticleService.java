package com.chhaya.mvvmapp.models.data.api.services;

import com.chhaya.mvvmapp.models.data.api.entities.Article;
import com.chhaya.mvvmapp.models.data.api.responses.ListArticleResponse;
import com.chhaya.mvvmapp.models.data.api.responses.SingleArticleResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ArticleService {

    @GET("v1/api/articles")
    Call<ListArticleResponse> getAll(@Query("page") long page, @Query("limit") long limit);

    @GET("v1/api/articles/{id}")
    Call<SingleArticleResponse> getOne(@Path("id") long id);

    @POST("v1/api/articles")
    Call<SingleArticleResponse> insertOne(@Body Article article);

    @PUT("v1/api/articles/{id}")
    Call<SingleArticleResponse> updateOne(@Path("id") long id, @Body Article article);

}
