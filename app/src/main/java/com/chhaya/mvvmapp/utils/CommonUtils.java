package com.chhaya.mvvmapp.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.chhaya.mvvmapp.R;

public class CommonUtils {

    private static Dialog progressDialog = null;
    private static ProgressBar progressBar = null;
    private static TextView progressText = null;

    public static void showPopupProgressSpinner(Context context, boolean isShowing) {
        if (isShowing) {
            progressDialog = new Dialog(context);
            progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            progressDialog.setContentView(R.layout.popup_progressbar);
            progressDialog.setCancelable(false);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            //progressBar = progressDialog.findViewById(R.id.progress_bar);
            //progressBar.getIndeterminateDrawable().setColorFilter(Color.parseColor("#ff6700"), PorterDuff.Mode.MULTIPLY);

            //progressText = progressDialog.findViewById(R.id.progress_text);
            //progressText.setText("Error Connection");

            progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }

}
