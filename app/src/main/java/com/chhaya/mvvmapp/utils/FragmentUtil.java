package com.chhaya.mvvmapp.utils;


import androidx.annotation.IdRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class FragmentUtil {
    
    public static void add(FragmentActivity context, @IdRes int container, Fragment fragment) {
        FragmentManager fm = context.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(container, fragment);
        ft.commit();
    }
    
    public static void replace(FragmentActivity context, @IdRes int container, Fragment fragment) {
        FragmentManager fm = context.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(container, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }
    
}
